## Komentáře, Profesionalismus, Flyweight

Bc.Vojtěch Žák

### Úvod

Programování je disciplína, která vyžaduje nejen technické dovednosti, ale i správný přístup a metodiku. Tato práce se zaměřuje na tři  aspekty programování: komentáře, profesionalismus a návrhový vzor Flyweight. Tyto tři aspekty nepřímo pokryjí kompletní doménu programování, kterou tvoří samotní lidé (vývojáři), napsaný kód a metodiky vývoje.

---

### 1. Komentáře

Komentáře v kódu jsou často diskutovaným tématem mezi vývojáři. Jsou nezbytné pro srozumitelnost a údržbu kódu, ale zároveň mohou být zdrojem nedorozumění a nejasností, pokud nejsou správně používány. 

>Good comments do not excuse unclear code.

![comments](https://cdn.stackoverflow.co/images/jo7n4k8s/production/414a8fbdab9119e924482a44f8c314a09e388768-1200x630.png?w=1200&h=630&auto=format&dpr=2)

Source: https://stackoverflow.blog/2021/12/23/best-practices-for-writing-code-comments/

#### 1.1. Clean Code angle

**Robert C. Martin** ve své knize "Clean Code" zdůrazňuje, že komentáře by měly být používány co nejméně. Nejlepší kód je takový, který je srozumitelný bez nutnosti komentářů. Martin uvádí několik typů špatných komentářů, kterým bychom se měli ve svém kódu vyhýbat:

- **Redundantní komentáře**: Komentáře, které jen opakují to, co je zřejmé z kódu.
- **Mumbling komentáře**: Nejasné a nedostatečně vysvětlené komentáře.
- **Noise komentáře**: Komentáře, které přidávají pouze šum a nepřinášejí žádnou hodnotu [Bogdan Poplauschi](https://bpoplauschi.github.io/2021/01/20/Clean-Code-Comments-by-Uncle-Bob-part-2.html) [LMU Computer Science](https://cs.lmu.edu/~ray/notes/cleancode/).

Martin doporučuje, aby byly komentáře použity k vysvětlení záměru nebo kontextu, které není možné jasně vyjádřit přímo v kódu. Například vysvětlení složitého algoritmu nebo obchodní logiky může být užitečné, ale komentáře by nikdy neměly opakovat to, co už je zjevné z kódu samotného.

Komentáře by měly být výstižné a poskytovat užitečný kontext. Komentáře kód nepíšeme proto, abychom vysvětlili, co kód dělá (to by měl čitelný kód ukázat sám), ale proto, abychom vysvětlili, proč to dělá. Martin uvádí, že „nic nemůže být tak užitečné jako dobře umístěný komentář, ale nic nemůže být tak škodlivé jako starý, špinavý komentář, který šíří lži a dezinformace“[LMU Computer Science](https://cs.lmu.edu/~ray/notes/cleancode/).

Dalším důležitým aspektem, který Martin zmiňuje, je **konzistence** v komentování. Komentáře by měly být konzistentní ve stylu a umístění. To znamená, že pokud se rozhodnete používat komentáře pro určité typy informací, měli byste tento přístup dodržovat v celém kódu.

#### 1.2. Is there another way?

Někteří vývojáři zastávají názor, že komentáře jsou vždy zbytečné a kód by měl být natolik čitelný, že žádné komentáře nepotřebuje. Tento přístup může vést k nadměrnému používání složitých názvů proměnných a funkcí, což může být kontraproduktivní. Například na fórech jako Reddit a Stack Overflow se často setkáváme s názory, že komentáře odvádějí pozornost od samotného kódu a mohou být dokonce matoucí, pokud nejsou pravidelně aktualizovány [Bogdan Poplauschi](https://bpoplauschi.github.io/2021/01/20/Clean-Code-Comments-by-Uncle-Bob-part-2.html).

Jedním z hlavních argumentů extrémních minimalistů je, že komentáře často zastarají a poskytují nepravdivé informace, což vede k více problémům než užitku. Tito vývojáři tvrdí, že kód by měl být napsán tak, aby byl sám o sobě srozumitelný a nepotřeboval žádné dodatečné vysvětlení.

Na druhou stranu, existují názory, že komentáře jsou klíčové pro dokumentaci kódu a sdílení znalostí v týmu, zejména pro nováčky nebo externí spolupracovníky. Tento přístup se často vyskytuje v korporátním prostředí, kde je třeba zajistit konzistenci a srozumitelnost kódu mezi velkým množstvím vývojářů. Komentáře mohou také sloužit jako důležitý nástroj pro dokumentaci složitých obchodních pravidel nebo logiky, kterou nemusí být snadné pochopit pouze z kódu.

#### 1.3. Examples  

**Dobře komentovaný kód**:
- Příklady kódu, kde jsou komentáře použity ke vysvětlení složitých algoritmů nebo obchodní logiky. Například:
  ```java
  // Tato metoda třídí seznam pomocí algoritmu Quicksort.
  // Používáme pivot a rozdělíme seznam na dvě části,
  // poté rekurzivně třídíme každou část zvlášť.
  public void quicksort(int[] array) {
      quicksort(array, 0, array.length - 1);
  }
  ```
  Tento komentář vysvětluje algoritmus třídění, což může být pro ostatní vývojáře velmi užitečné, pokud nejsou s tímto algoritmem obeznámeni.

- Další příklad může být vysvětlení obchodní logiky:
  ```python
  # Tato funkce počítá daň z příjmu na základě zadaného platu.
  # Používáme progresivní daňovou sazbu, která se mění v závislosti na výši příjmu.
  def calculate_income_tax(income):
      if income <= 10000:
          return income * 0.1
      elif income <= 20000:
          return 1000 + (income - 10000) * 0.2
      else:
          return 3000 + (income - 20000) * 0.3
  ```

**Špatně komentovaný kód**:
- Příklady, kde jsou komentáře nadbytečné nebo zavádějící:
  ```java
  // Zvýší proměnnou i o 1
  i++;
  ```
  V tomto případě je komentář naprosto zbytečný, protože kód je sám o sobě zcela srozumitelný.

- Další příklad špatného komentáře může být:
  ```java
  // Tato proměnná obsahuje počet položek
  int count = 10;
  ```
  Tento komentář opakuje to, co je zřejmé z názvu proměnné a její hodnoty. Místo toho by bylo lepší použít srozumitelnější název proměnné, např. `numberOfItems`.

#### 1.4 Reccomendations for writing comments

Na základě výše uvedených informací je možné formulovat několik doporučení pro psaní efektivních komentářů:
- **Používejte komentáře střídmě**: Komentáře by měly být použity jen tehdy, když jsou skutečně potřeba. Kód by měl být srozumitelný i bez nich.
- **Vysvětlujte důvody, ne akce**: Komentáře by měly vysvětlovat, proč něco děláte, nikoliv co děláte. To by mělo být zřejmé z kódu samotného.
- **Udržujte komentáře aktuální**: Pokud změníte kód, ujistěte se, že aktualizujete i komentáře. Staré a nepravdivé komentáře mohou být matoucí.
- **Používejte konzistentní styl**: Komentáře by měly být konzistentní ve svém stylu a umístění. To usnadní jejich čtení a pochopení.
- **Vyhněte se redundantním komentářům**: Nepište komentáře, které opakují to, co je zřejmé z kódu. Tyto komentáře jsou zbytečné a mohou ztěžovat čitelnost kódu.

![comments_joke](https://cdn.stackoverflow.co/images/jo7n4k8s/production/4ad7a0bd338d7616a7900b9c94b7bd2c53da825e-794x590.png?auto=format)

Source: https://www.reddit.com/r/ProgrammerHumor/comments/bz35nh/whats_a_comment/


---

### 2. Practicing (Profesionalismus)

Profesionalismus v programování není jen o psaní funkčního kódu, ale zahrnuje širokou škálu dovedností a přístupů, které zajišťují kvalitu, efektivitu a udržitelnost softwarového vývoje. Primárně se netýká kódu jako takového, ale lidí a jejich dovedností, kteří se na vzniku software podílejí.

#### 2.1. The Clean Coder

**Robert C. Martin** ve své knize "The Clean Coder" klade důraz na profesionalismus, který zahrnuje zodpovědnost, integritu a neustálé učení. Profesionální programátor by měl být zodpovědný za svůj kód, dodržovat sliby a neustále se zdokonalovat ve svých dovednostech[LMU Computer Science](https://cs.lmu.edu/~ray/notes/cleancode/). Martin rozebírá několik klíčových oblastí, které jsou zásadní pro profesionální chování v IT:

- **Zodpovědnost**: Programátoři by měli být zodpovědní za kvalitu svého kódu. To zahrnuje psaní čitelného a udržovatelného kódu, testování kódu a jeho pravidelnou refaktorizaci. Zodpovědnost znamená také schopnost přiznat chyby a napravit je co nejdříve.

- **Integrita**: Dodržování slibů a být čestní ohledně svých schopností a časových možností. Například pokud vývojář slíbí, že dodá určitou funkcionalitu do určitého termínu, měl by tento závazek dodržet. Integrita znamená také dodržování etických zásad a nepřijímání zkratek, které by mohly vést k nekvalitnímu kódu.

- **Nepřetržité učení**: Neustálé zdokonalování svých dovedností a učení se novým technologiím. Martin doporučuje, aby vývojáři pravidelně četli knihy, účastnili se konferencí a sledovali novinky v oboru. Profesionalismus znamená být otevřený novým nápadům a technikám a být ochoten se učit od ostatních.

Martin také zdůrazňuje význam **komunikace** a **spolupráce** v týmu. Profesionální vývojář by měl být schopen efektivně komunikovat se svými kolegy, sdílet znalosti a řešit konflikty konstruktivně. Kvalitní komunikace zahrnuje také schopnost naslouchat a porozumět potřebám a požadavkům ostatních členů týmu a zákazníků.

![soft_hard](https://jaydevs.com/wp-content/uploads/2021/12/1-16.jpg)

Source: https://jaydevs.com/why-soft-skills-matter-when-hiring-a-software-developer/

#### 2.2. Arguments

Existují různé názory na to, co znamená být profesionálním vývojářem, a některé z těchto názorů jsou poměrně extrémní. 

- **Anti-profesionalismus**: Někteří vývojáři tvrdí, že profesionalismus je zbytečný a že vývojáři by měli být hodnoceni pouze podle svých technických dovedností, nikoli podle svého chování nebo přístupu. Tento názor se často objevuje na diskuzních fórech a v komunitách zaměřených na čistě technické aspekty vývoje. Tito vývojáři argumentují, že výsledky by měly mluvit samy za sebe, a že byrokratické požadavky na profesionalitu jen zdržují práci.

- **Minimalismus a pragmatismus**: Jiní vývojáři zastávají názor, že mnoho doporučených praktik (např. TDD - Test-Driven Development) je přehnaných a nepřináší vždy přidanou hodnotu. Tvrdí, že je důležité najít rovnováhu mezi kvalitou a rychlostí vývoje a že přílišné zaměření na metodiky může vést k ztrátě flexibility a kreativity. Například někteří vývojáři upřednostňují pragmatický přístup k testování, kde se zaměřují na nejdůležitější části kódu a nepokouší se dosáhnout 100% pokrytí testy.

- **Extrémní profesionalita**: Na druhé straně spektra jsou zastánci přísných standardů a metodik, kteří věří, že dodržování vysokých standardů je klíčové pro úspěch v oboru. Tito vývojáři se drží přísných pravidel a metodik, jako je TDD, kontinuální integrace a refaktorizace. Argumentují, že tyto praktiky zajišťují dlouhodobou udržitelnost a zlepšují kvalitu softwaru.

#### 2.3. Implications

**Profesionální chování**:
- Příklady situací, kdy zodpovědný a profesionální přístup pomohl vyřešit konflikty v týmu nebo zvýšil produktivitu.
  - **Příklad**: V jednom týmu došlo ke konfliktu mezi dvěma vývojáři ohledně implementace nového feature. Profesionální přístup zahrnující otevřenou komunikaci a hledání kompromisu vedl k rychlému a efektivnímu řešení. Vývojáři si sedli a společně diskutovali o různých přístupech, nakonec našli kompromisní řešení, které vyhovovalo oběma stranám.

**Time management**:
- Ukázky efektivního řízení času a organizace práce, například používání metod jako Pomodoro Technique nebo time-blocking.
  - **Příklad**: Použití metody Pomodoro Technique, kdy programátor pracuje 25 minut a poté si udělá 5 minutovou přestávku, vedlo k zvýšení produktivity a snížení únavy. Tato technika pomáhá vývojářům udržet vysokou úroveň koncentrace a zároveň zamezuje vyhoření tím, že poskytuje pravidelné přestávky.

**Nepřetržité učení a rozvoj**:
- **Příklad**: Vývojář se rozhodne každý měsíc přečíst jednu odbornou knihu a účastnit se alespoň jedné konference ročně. Tento přístup mu pomáhá udržovat aktuální znalosti a rozšiřovat své dovednosti. Díky tomu může přinášet do svého týmu nové nápady a zlepšovat kvalitu kódu.

**Refaktorizace a kvalita kódu**:
- **Příklad**: Vývojář pravidelně provádí refaktorizaci kódu, aby zajistil jeho čitelnost a udržovatelnost. To zahrnuje odstraňování duplicitního kódu, zlepšování názvů proměnných a metod a optimalizaci algoritmů. Tento přístup zajišťuje, že kód zůstává čistý a snadno pochopitelný pro ostatní členy týmu.

**Etické rozhodování**:
- **Příklad**: Vývojář se rozhodne neimplementovat požadavek klienta, který by vedl k neetickému použití softwaru (například sledování uživatelů bez jejich souhlasu). Místo toho otevřeně komunikuje s klientem o svých obavách a navrhuje alternativní řešení, které splňuje požadavky klienta a zároveň je eticky přijatelné.

#### 2.4 Summary

Profesionalismus v programování je komplexní téma, které zahrnuje mnoho aspektů, od technických dovedností po měkké dovednosti jako komunikace a týmová spolupráce. Profesionální přístup k programování zahrnuje zodpovědnost, integritu, nepřetržité učení a schopnost efektivně řídit čas a zdroje. Ačkoliv existují různé názory na to, co znamená být profesionálním vývojářem, klíčové je udržovat vysoké standardy a neustále se zlepšovat, aby se zajistila kvalita a udržitelnost softwaru.

![bad_dev](https://www.explore-group.com/storage/images/Friday.jpg)

Source: https://www.explore-group.com/blog/top-10-developer-mistakes/bp59/


---

### 3. Flyweight (Návrhový vzor)

Návrhový vzor Flyweight je jedním z klíčových vzorů pro optimalizaci paměti v programování. Tento vzor umožňuje efektivní práci s velkým množstvím podobných objektů tím, že minimalizuje paměťovou náročnost prostřednictvím sdílení společných částí stavu. Tento pattern je známý především díky game developmentu, ale najde uplatnění i v mnoha jiných oblastech softwarových řešení.

![flyweight](https://miro.medium.com/v2/resize:fit:786/format:webp/0*442VyQ8IMnbkaXmY.png)

Source: https://medium.com/@rajeshvelmani/lightweight-objects-for-efficient-performance-exploring-the-flyweight-design-pattern-in-java-4595ebfa3165

#### 3.1. Literature (Game Programming Patterns)

**Robert Nystrom** ve své knize "Game Programming Patterns" popisuje Flyweight jako vzor, který umožňuje úsporu paměti při práci s velkým množstvím podobných objektů. Tento vzor sdílí stav mezi více objekty a odděluje vnitřní a vnější stav. Nystrom zdůrazňuje, že Flyweight je užitečný zejména v situacích, kde je potřeba spravovat velké množství jemně granulovaných objektů, například v herním vývoji pro správu textur nebo spriteů [LMU Computer Science](https://cs.lmu.edu/~ray/notes/cleancode/).

Principy Flyweight vzoru zahrnují:
- **Oddělení vnitřního a vnějšího stavu**: Vnitřní stav je sdílený mezi všemi instancemi Flyweight, zatímco vnější stav je specifický pro každou instanci a je předáván metodám Flyweight.
- **Sdílení vnitřního stavu**: Vnitřní stav je uložen pouze jednou a sdílen mezi více instancemi, čímž se šetří paměť.

Nystrom uvádí, že Flyweight je vhodný pro situace, kde je potřeba spravovat velké množství objektů, které sdílejí většinu svého stavu. Typickými příklady jsou herní objekty jako textury, kde může být stejný obrázek použit pro mnoho různých objektů.

#### 3.2. Is it a good idea?

Někteří vývojáři tvrdí, že Flyweight je příliš složitý a náročný na implementaci. Preferují jednodušší vzory nebo řešení, která jsou méně efektivní z hlediska paměti, ale snazší na implementaci a údržbu. Tito vývojáři argumentují, že Flyweight může přinést komplikace do návrhu systému a že úspory paměti nemusí vždy stát za zvýšenou složitost kódu [LMU Computer Science](https://cs.lmu.edu/~ray/notes/cleancode/).

Na druhé straně jsou názory, že Flyweight je v určitých případech nezbytný, například při vývoji her, kde je paměť velmi omezený zdroj. V těchto případech může Flyweight výrazně zlepšit výkon a efektivitu aplikace. Někteří zastánci Flyweight vzoru tvrdí, že správné použití tohoto vzoru může vést k výrazným úsporám paměti a zlepšení celkového výkonu aplikace, což je klíčové zejména v prostředích s omezenými zdroji, jako jsou mobilní zařízení nebo vestavěné systémy.

#### 3.3. Game design examples

**Implementace Flyweight**:
- Ukázky použití vzoru ve hrách pro správu textur nebo spriteů.
  - **Příklad**: V jedné hře byla implementována Flyweight metoda pro správu textur, která umožnila výrazně snížit paměťovou náročnost a zvýšit výkon hry. Každý objekt ve hře sdílel stejnou instanci textury, čímž se snížila celková paměťová stopa aplikace.
  ```java
  public class TextureFlyweight {
      private static final Map<String, Texture> textures = new HashMap<>();

      public static Texture getTexture(String textureName) {
          Texture texture = textures.get(textureName);
          if (texture == null) {
              texture = loadTexture(textureName);
              textures.put(textureName, texture);
          }
          return texture;
      }

      private static Texture loadTexture(String textureName) {
          // Logika načítání textury z disku nebo jiného zdroje
          return new Texture(textureName);
      }
  }
  ```

**Porovnání výkonu**:
- Srovnání efektivity paměti a výkonu při použití Flyweight vzoru oproti jiným přístupům.
  - **Příklad**: Srovnání ukázalo, že použití Flyweight vzoru vedlo ke snížení paměťové náročnosti o 30 % ve srovnání s tradičními metodami, kde každá instance objektu měla svou vlastní texturu. Tento přístup nejenže snížil paměťovou stopu aplikace, ale také zrychlil načítání a vykreslování objektů ve hře.
  ```python
  class FlyweightFactory:
      _flyweights = {}

      @classmethod
      def get_flyweight(cls, key):
          if key not in cls._flyweights:
              cls._flyweights[key] = Flyweight(key)
          return cls._flyweights[key]

  class Flyweight:
      def __init__(self, shared_state):
          self._shared_state = shared_state

      def operation(self, unique_state):
          s = self._shared_state
          u = unique_state
          print(f"Flyweight: Displaying shared ({s}) and unique ({u}) state.")

  # Použití
  factory = FlyweightFactory()

  flyweight1 = factory.get_flyweight("TextureA")
  flyweight2 = factory.get_flyweight("TextureB")
  flyweight3 = factory.get_flyweight("TextureA")  # Sdílí stejný stav jako flyweight1

  flyweight1.operation("Object1")
  flyweight2.operation("Object2")
  flyweight3.operation("Object3")
  ```

Tyto příklady ukazují, jak lze Flyweight vzor efektivně použít pro správu zdrojů a optimalizaci paměti v aplikacích. V situacích, kde je potřeba spravovat velké množství podobných objektů, může Flyweight vzor přinést významné úspory paměti a zlepšení výkonu.

---

#### 3.4 Summary

Návrhový vzor Flyweight je silným nástrojem pro optimalizaci paměti v programování, zejména v prostředích s omezenými zdroji. Přestože může být jeho implementace složitá a náročná, přináší výrazné úspory paměti a zlepšení výkonu v aplikacích, kde je potřeba spravovat velké množství podobných objektů. Tato sekce prozkoumala principy Flyweight vzoru, různé názory na jeho použití a praktické příklady implementace. Správné použití tohoto vzoru může výrazně zlepšit efektivitu a udržitelnost softwarových aplikací.

---

## Seznam použité literatury

 * Robert C. Martin (2008), Clean Code: 1 <https://moodle.utb.cz/pluginfile.php/1134729/mod_page/content/3/clean_code.pdf>
 * Bogdan Poplauschi (2021), Clean Code - Comments <https://bpoplauschi.github.io/2021/01/20/Clean-Code-Comments-by-Uncle-Bob-part-2>
 * StackOverflow (2021), Best practices for writing comments <https://stackoverflow.blog/2021/12/23/best-practices-for-writing-code-comments/>
 * Evgeniy Fetisov (2021), Why soft skills matter <https://jaydevs.com/why-soft-skills-matter-when-hiring-a-software-developer/>

